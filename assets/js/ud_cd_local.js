
// Using this approach until PHP solution is implemented
var TIAA_ud = TIAA_ud || {};

TIAA_ud = {
	udThemeUrl: '/themes/ud_atom/release_2013-08/',
	globalAssetUrl: '/assets/',
	globalThemeUrl: '/assets/release_2013-08/',
	localUrl: 'assets/',
	oneColWidth: 1200,
	twoColWidth: 940

};