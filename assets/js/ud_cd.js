jQuery(document).ready(function($) {
	content.load();
});

var content = function() {
		var source = $('<div>');
		function load() {
			source.load('content.html', function() {
				fill();
			});
		};

		function get(id) {
			var el = $('#' + id, source);
			// return a copy of the item's contents as a jquery object
			return el.clone().children().unwrap();
		};

		function fill() {
			$(document).trigger('dc-before-load');
			var dynamicContent = $('.dynamic-content');
			dynamicContent.each(function(i, item) {
				var key = $(item).data('content-key');
				var el = $('#' + key, source);
				if(el.length) {
					$(item).replaceWith(el.html());
				}
			});
			$(document).trigger('dc-after-load');
		};
		return {
			source: source,
			load: load,
			get: get
		};
	}(); /* function for creating udlayout */
	
	